import java.util.ArrayList;
import java.util.List;

import model.Fato;
import model.Schema;
import util.SimulacaoDAO;

public class Main {

	public static void main(String[] args) {

		List<Schema> schemas = SimulacaoDAO.retornaListaSchemaPreenchido();
		List<Fato> fatos = SimulacaoDAO.retornaListaPreenchidaFatos();

		List<Fato> resultados = new ArrayList<>();

		for (Fato fato : fatos) {
			if (fato.isInformacaoAtual()) {

				for (Schema schema : schemas) {
					boolean contemFato = false;
					Fato aux = null;
					if (fatoIsMany(fato, schema) && atributoIgual(fato, schema)) {
						resultados.add(fato);
						continue;
					} else if (resultados.isEmpty()) {
						resultados.add(fato);
						continue;
					} else if (listaNaoContemFato(fato, resultados)) {
						resultados.add(fato);
						continue;
					} else if (!fatoIsMany(fato, schema) && atributoIgual(fato, schema)) {
						for (Fato resultado : resultados) {
							if (resultado.getAtributo().equals(fato.getAtributo()) && resultado.getEntidade().equals(fato.getEntidade())) {
								contemFato = true;
								aux = resultado;
							}
						}

						if (contemFato) {
							resultados.remove(aux);
							resultados.add(fato);
						}
					}
				}
			}
		}

		System.out.println(resultados);
	}

	private static boolean atributoIgual(Fato fato, Schema schema) {
		return schema.getAtributo().equals(fato.getAtributo());
	}

	private static boolean listaNaoContemFato(Fato fato, List<Fato> resultados) {
		boolean fatoNaoExiste = true;
		for (Fato resultado : resultados) {
			if (resultado.getEntidade().equals(fato.getEntidade())) {
				fatoNaoExiste = false;
			}
		}
		
		return fatoNaoExiste;
	}

	private static boolean fatoIsMany(Fato fato, Schema schema) {
		return (schema.getValor().equals("many"));
	}

}
