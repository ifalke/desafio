package util;

import java.util.ArrayList;
import java.util.List;

import model.Fato;
import model.Schema;

public class SimulacaoDAO {

	public static List<Fato> retornaListaPreenchidaFatos() {
		
		List<Fato> fatos = new ArrayList<>();
		
		fatos.add(new Fato("gabriel", "endere�o", "av rio branco, 109", true));
		fatos.add(new Fato("jo�o", "endere�o", "rua alice, 10", true));
		fatos.add(new Fato("jo�o", "endere�o", "rua bob, 88", true));
		fatos.add(new Fato("jo�o", "telefone", "234-5678", true));
		fatos.add(new Fato("jo�o", "telefone", "91234-5555", true));
		fatos.add(new Fato("jo�o", "telefone", "234-5678", false));
		fatos.add(new Fato("gabriel", "telefone", "98888-1111", true));
		fatos.add(new Fato("gabriel", "telefone", "56789-1010", true));
		
		return fatos;
	}
	
	public static List<Schema> retornaListaSchemaPreenchido() {
		List<Schema> schemas = new ArrayList<>();
		
		schemas.add(new Schema("endere�o", "cardinality", "one"));
		schemas.add(new Schema("telefone", "cardinality", "many"));
		
		return schemas;
	}
}
