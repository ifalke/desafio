package model;

public class Fato {

	private String entidade;
	private String atributo;
	private String valor;
	private boolean informacaoAtual;

	public Fato() {
		super();
	}

	public Fato(String entidade, String atributo, String valor, boolean informacaoAtual) {
		super();
		this.entidade = entidade;
		this.atributo = atributo;
		this.valor = valor;
		this.informacaoAtual = informacaoAtual;
	}

	public String getEntidade() {
		return entidade;
	}

	public void setEntidade(String entidade) {
		this.entidade = entidade;
	}

	public String getAtributo() {
		return atributo;
	}

	public void setAtributo(String atributo) {
		this.atributo = atributo;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public boolean isInformacaoAtual() {
		return informacaoAtual;
	}

	public void setInformacaoAtual(boolean informacaoAtual) {
		this.informacaoAtual = informacaoAtual;
	}

	@Override
	public String toString() {
		return "Fato [entidade=" + entidade + ", atributo=" + atributo + ", valor=" + valor + ", informacaoAtual="
				+ informacaoAtual + "]";
	}

}
