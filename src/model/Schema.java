package model;

public class Schema {

	private String atributo;
	private String cardinalidade;
	private String valor;

	public Schema() {
		super();
	}

	public Schema(String atributo, String cardinalidade, String valor) {
		super();
		this.atributo = atributo;
		this.cardinalidade = cardinalidade;
		this.valor = valor;
	}

	public String getAtributo() {
		return atributo;
	}

	public void setAtributo(String atributo) {
		this.atributo = atributo;
	}

	public String getCardinalidade() {
		return cardinalidade;
	}

	public void setCardinalidade(String cardinalidade) {
		this.cardinalidade = cardinalidade;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

}
